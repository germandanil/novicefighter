import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if(fighter) {
      const preview___content = createElement({
        tagName: 'div',
        className: `fighter-preview___content`,
      });
      const preview__Data_content = createElement({
        tagName: 'div',
        className: `preview__Data_content`,
      });
  
      const name = createElement({
        tagName: 'h2',
        className: `name`,
      });
      name.innerText = fighter.name;
  
      const HP = createElement({
        tagName: 'div',
        className: `HP`,
      });
      HP.innerText = `HP = ${fighter.health}`
  
      const Attack = createElement({
        tagName: 'div',
        className: `attack`,
      });
      Attack.innerText = `Attack = ${fighter.attack}`
  
      const Defense = createElement({
        tagName: 'div',
        className: `defense`,
      });
      Defense.innerText = `Defense = ${fighter.defense}`
      const fighterImg = createFighterImage(fighter);
      preview__Data_content.append(name,HP,Attack,Defense)
      preview___content.append(preview__Data_content,fighterImg,);
      fighterElement.append(preview___content);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const fighterImg = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return fighterImg;
}